import click
from cli._pack import pack
from cli._test import test

@click.group()
def cli():
    pass    

cli.add_command(pack)   
cli.add_command(test)

if __name__ == '__main__':
    cli()