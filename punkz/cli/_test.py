import click

@click.group()
def test():
    pass

@click.command()
@click.option("--opt", "-o", help="Test option", prompt="Test option", required=True, type=click.Choice(["a", "b", "c"]))
def test_func(opt: str):
    click.echo("Option selected:", opt)

test.add_command(test_func)