import click
from cli.context import cache, compress, decompress

@click.group()
def pack():
    pass

@click.command()
@click.option("--file_path", "-f", help="File to compress", prompt="File to compress", required=True, type=click.Path(exists=True, dir_okay=False, readable=True, resolve_path=True))
def compress_file(file_path: str) -> None:
    with open(file_path, "r") as file:
        data = file.read()
        compressed_data = compress(data)
        with open(file_path + ".punkz", "wb") as compressed_file:
            compressed_file.write(compressed_data)

@click.command()
@click.option("--file_path", "-f", help="File to decompress", prompt="File to decompress", required=True, type=click.Path(exists=True, dir_okay=False, readable=True, resolve_path=True))
def decompress_file(file_path: str) -> None:
    with open(file_path, "rb") as file:
        data = file.read()
        decompressed_data = decompress(data)
        with open(file_path + ".decompressed", "w") as decompressed_file:
            decompressed_file.write(decompressed_data)

pack.add_command(compress_file)
pack.add_command(decompress_file) 

if __name__ == '__main__':
    pack()