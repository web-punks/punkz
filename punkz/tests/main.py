from context import cache

@cache
def test(x):
    return x*x

if __name__ == "__main__":
    input_ = [1,2,3,4,5]

    for i in input_:
        test(i)

