from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor, wait, as_completed

class Accelerate:
    def __init__(self) -> None:
        self.executor = ProcessPoolExecutor(max_workers=4)

    def multiprocess(self, func, *args, **kwargs):
        print("multiprocess method called")
        def wrapper(*args, **kwargs):
            return self.executor.submit(func, *args, **kwargs)
        return wrapper

accelerate = Accelerate() 

if __name__ == "__main__":
    from time import sleep

    @accelerate.multiprocess
    def f(x):
        sleep(3)
        return x*x
    
    input_ = [
        [1, 2, 3, 4, 5],
        [6, 7, 8, 9, 10]
    ]

    futures = [f(i) for i in input_[0]]
    
    done, not_done = wait(futures, timeout=10, return_when='ALL_COMPLETED')

    print(done)
    print(not_done)
    
    # res = [f.result() for f in futures]

    # print(res)



    # with ProcessPoolExecutor as executor:
    #     futures = [executor.submit(f, x) for x in input_]
    #     results = [f.result() for f in futures]
    #     print(results)