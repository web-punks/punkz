print("\t\t\trunning __init__ utils")
print("\t\t\t\timport cache")
from punkz.utils.cache import cache
print("\t\t\t\t cache imported")
print("\t\t\t\timport compress")
from punkz.utils.compression import compress, decompress
print("\t\t\t\t compress imported")
print("\t\t\tending __init__ utils")