from typing import Any, Callable
from punkz.utils.compression import compress, decompress
from ast import literal_eval
import os
import hashlib


def _sha256_hash(string: str) -> str:
    """
    Get sha256 hash of a string.
    Parameters:
        string (str): string to be hashed.
    Returns:
        hash (str): sha256 hash of the string.
    """
    hash = hashlib.sha256()
    hash.update(string.encode("utf-8"))
    return hash.hexdigest()


def _get_hash_key(func_name: str, *args, **kwargs) -> str:
    """
    Get hash key for a function call.
    Parameters:
        *args (list): list of arguments.
        **kwargs (dict): dictionary of keyword arguments.
    Returns:
        hash_key (str): hash key for the function call.
    """
    hash_key: str = func_name
    for arg in args:
        hash_key += str(arg)
    for key, value in kwargs.items():
        hash_key += str(key) + str(value)
    return hash_key


def _get_cache_dir_name(func_name: str) -> str:
    """
    Create cache directory path for a function call.
    Parameters:
        func_name (str): name of the function.
    Returns:
        cache_dir (str): cache directory for the function call.
    """
    cache_name = func_name + "_cache"
    return os.path.join(os.getcwd(), cache_name)


def _get_cache(func_name: str) -> str:
    """
    Get cache directory for a function call.
    Parameters:
        func_name (str): name of the function.
    Returns:
        cache_dir (str): cache directory for the function call.
    """
    cache_dir = _get_cache_dir_name(func_name)
    if os.path.exists(cache_dir):
        return cache_dir
    else:
        os.mkdir(cache_dir)
        return cache_dir

def _parse_types(res: str) -> Any:
    """
    Parse the type of the result.
    Parameters:
        res (str): result of the function call.
    Returns:
        The parsed result.
    """
    try:
        parsed_res = literal_eval(res)
    except:
        parsed_res = res
    
    return parsed_res

def cache(func) -> Callable:
    """
    Decorator to cache the results of a function call.
    Parameters:
        func (function): function to be cached.
        cache_dir (str): directory to store the cached results.
    Returns:
    """

    def wrapper(*args, **kwargs):
        hash_key = _sha256_hash(_get_hash_key(func.__name__, *args, **kwargs))
        cache_dir = _get_cache(func.__name__)

        if hash_key and cache_dir:
            file_name = hash_key + ".br"
            file_path = os.path.join(os.getcwd(), cache_dir, file_name)
            if os.path.exists(file_path):
                with open(file_path, "rb") as f:
                    res = _parse_types(decompress(f.read()))
            else:
                res = func(*args, **kwargs)
                with open(file_path, "wb") as f:
                    f.write(compress(str(res)))
            return res
        return func(*args, **kwargs)

    return wrapper


