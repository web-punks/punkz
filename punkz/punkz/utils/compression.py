import brotli

def compress(data: str) -> bytes:
    return brotli.compress(data.encode("utf-8"))

def decompress(data: bytes) -> str:
    return brotli.decompress(data).decode("utf-8")